/**
 * A rain game
 *
 * <p>
 * The objective of this game is to determine the amount of rain in each pool at
 * the end of the rain season.
 *
 * <p>
 * Imagine an area on which it rains during a certain period. This area is
 * however not flat but has hills and valleys. It rains on the whole area, but
 * of course the water accumulates in the lowest parts. We want to know how much
 * water is in each pool after the rain stops.
 *
 * <p>
 * The game starts on a rectangular playing field which has already been divided
 * into smaller areas (squares if you will). Of these areas the altitude is
 * given as well. When it rains, the water will flow from the highest altitudes
 * to the lowest altitude. Pools form where the (regional) lowest points are.
 * Initially, it is assumed that the rain is uniform over the entire area, so
 * each smaller area receives the same amount of rain.
 *
 * <p>
 * The rules of the flow are as follows: -- water flows from high to low
 * regions. It never flows from low to high regions. -- water follows the
 * steepest incline, meaning that a region with an altitude of 8 next to a two
 * regions of 3 and 2 (resp.) will flow towards 2. -- if two inclines should be
 * equally steep, the flow chooses the first of North - East - South - West. --
 * water flows only in east-west (/west-east) and north-south (/south-north)
 * direction. It does not flow diagonally over the region. -- if the altitude of
 * all neighbors is equal, there is no flow.
 *
 */
package nl.cavero.raingame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * rectangular playing field array Squares filled with Square objects Square
 * objects created using the constructor supplied heightMap heightMap is a
 * 2Dimensional Array. In other words, a x,y map
 *
 */
public class Area {

    private final List<Square> squares = new ArrayList<>();
    private final int[][] heightMap;
    private final int amountOfRainPerSquare;
    private final int[] resultSetWaterLevel;
    
    /**
     * initializes the Area. Rain is not yet set.
     *
     * @param heightMap:  int[][] = height matrix met height values representing
     * the area.
     * @param amountOfRainPerSquare
     */
    public Area(int[][] heightMap, int amountOfRainPerSquare) {
        this.heightMap = heightMap;
        this.amountOfRainPerSquare = amountOfRainPerSquare;
        createSquares();
        initNeighbours();
        this.resultSetWaterLevel = new int[squares.size()];
    }

    /**
     * fill squares ArrayList with created initialized Square objects
     */
    private void createSquares() {
        int y = 0;
        int x = 0;

        for (int[] col : heightMap) {
            for (int height : col) {
                Square square = new Square(x, y, height);
                Water water = new Water(square);
                water.setHeight(this.getAmountOfRainPerSquare());
                square.setWater(water);
                squares.add(square);
                y++;
            }
            x++;
            y = 0;
        }
    }

    /**
     * eacht Square object has 4 neighbours. Determines if the square has a
     * neighbour, if so, the neighbour is set.
     */
    private void initNeighbours() {
        for (Square square : squares) {
            setNeighbourNorth(square);
            setNeighbourSouth(square);
            setNeighbourWest(square);
            setNeighbourEast(square);
            square.initNeighbours();
        }
    }

    public void letItFlow() {
        //iterate thru squares, based on heightest square.
        //heigest square is the square elevation plus waterheight.
        //let it flow per square.

        //Highest waterlevel sorted on top 
        Collections.sort(squares);
        //Niels: functional operator???
        int count = 0;
        int elementCount = 0;
        int[] resultWaterLevel = new int[squares.size()];
        
        do {
            for (Square square : squares) {
                
                System.out.println("----------------------------------------");
                System.out.printf("Prior flow(): "
                        + "Iteration: %d; "
                        + "square coordinates: %d,%d; "
                        + "square height: %d; "
                        + "water height: %d; "
                        + "square waterlevel: %d%n",
                        count,
                        square.getX(),
                        square.getY(),
                        square.getHeight(),
                        square.getWater().getHeight(),
                        square.getWaterLevel());

                Square destSquare = square.getWater().flow();
                resultWaterLevel[elementCount] = square.getWaterLevel();
                
                System.out.printf("Post flow(): "
                        + "square coordinates: %d,%d; "
                        + "square height: %d; "
                        + "water height: %d; "
                        + "square waterlevel: %d%n",
                        square.getX(),
                        square.getY(),
                        square.getHeight(),
                        square.getWater().getHeight(),
                        square.getWaterLevel());
                
                if (destSquare != null) {
                    System.out.printf("Square that received the water flow(): "
                            + "square coordinates: %d,%d; "
                            + "square height: %d; "
                            + "water height: %d; "
                            + "square waterlevel: %d%n",
                            destSquare.getX(),
                            destSquare.getY(),
                            destSquare.getHeight(),
                            destSquare.getWater().getHeight(),
                            destSquare.getWaterLevel());
                }
                elementCount++;
            }
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");            
            elementCount = 0;
            count++;
        } while (equilibriumReached(resultWaterLevel) == false); 
        
    }

    /**
     * Compare coordinates of the square's in array squares. 
     * Neighbour is: (x, y-1)
     *
     * @param square The square of which the NeighbourNorth property will be set
     */
    private void setNeighbourNorth(Square square) {
        for (Square squareNorth : squares) {
            if ((squareNorth.getX() == square.getX())
                    && (squareNorth.getY() == (square.getY() - 1))) {
                square.setNorth(squareNorth);
            }
        }
    }

    /**
     * Compare coordinates of the square's in array squares. Neighbour is: (x,
     * y+1)
     *
     * @param square The square of which the NeighbourSouth property will be set
     */
    private void setNeighbourSouth(Square square) {
        for (Square squareSouth : squares) {
            if ((squareSouth.getX() == square.getX())
                    && (squareSouth.getY() == (square.getY() + 1))) {
                square.setSouth(squareSouth);
            }
        }
    }

    /**
     * Compare coordinates of the square's in array squares. Neighbour is: 
     * (x-1, y)
     *
     * @param square The square of which the NeighbourWest property will be set
     */
    private void setNeighbourWest(Square square) {
        for (Square squareWest : squares) {
            if ((squareWest.getX() == square.getX() - 1)
                    && (squareWest.getY() == square.getY())) {
                square.setWest(squareWest);
            }
        }
    }

    /**
     * Compare coordinates of the square's in array squares. Neighbour is: (x+1,
     * y)
     *
     * @param square The square of which the NeighbourWest property will be set
     */
    private void setNeighbourEast(Square square) {
        for (Square squareEast : squares) {
            if ((squareEast.getX() == square.getX() + 1)
                    && (squareEast.getY() == square.getY())) {
                square.setEast(squareEast);
            }
        }
    }

    /**
     * @return the amountOfRainPerSquare
     */
    public int getAmountOfRainPerSquare() {
        return amountOfRainPerSquare;
    }

    /**
     * @return the resultSetWaterLevel
     */
    private int[] getResultSetWaterLevel() {
        return resultSetWaterLevel;
    }

    /**
     * 
     * @param resultSetWaterLevel WaterLevel is Square.height plus Water.height.
     * The resultset of 1 iteration over the Area is stored in the 
     * resultSetWaterLevel array. After each iteration the resultset in stored
     * and compared with the results of the previous results. If the current
     * result and previous result is equall an 
     * @return if a equilibrium has been reached equilibrium has been reached.
     */
    private boolean equilibriumReached(int[]resultSetWaterLevel) {
        for(int i = 0; i < this.resultSetWaterLevel.length; i++) {
            if(this.getResultSetWaterLevel()[i] != resultSetWaterLevel[i]) {
                for(int j = 0; j < resultSetWaterLevel.length; j++) {
                    this.resultSetWaterLevel[j] = resultSetWaterLevel[j]; 
                }
                return false;
            }
        }
        return true;
    }
    
    
    /**
     * A pool is considered a pool when object water has height > 0.
     * @return list with Pool objects
     */
    public List<Pool> getPools() {
        Test.StringComparatorStaticInnerClass istaticclass = new Test.StringComparatorStaticInnerClass();
        Test.StringComparatorInnerClass iclass = new Test().new StringComparatorInnerClass();
        
        
        List<Pool> pools = new ArrayList<>();
        for(Square square : squares) {
            if(square.getWater().getHeight() > 0) {
                Pool pool = new Pool(square.getX(), square.getY(), 
                        square.getWater().getHeight());
                pools.add(pool);
            }
        }
        return pools;
    }
    
    static void doSomething(Area area) {
        System.err.println("" + area.squares); 
    }
    
}
