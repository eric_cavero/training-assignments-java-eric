package nl.cavero.raingame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * rectangular playing field which has already been divided into smaller areas.
 * These smaller areas we call a Square.
 *
 */
public class Square implements Comparable<Square> {
    private Water water;
    final private int x;
    final private int y;
    final private int z;
    private Square north;
    private Square south;
    private Square west;
    private Square east;
    private List<Square> neighbours;

    /**
     *
     * @param x
     * @param y
     * @param z
     *
     */
    public Square(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Determines the steepest incline value
     *
     * @return the steepest incline value, returns 0 is there is no incline
     */
    private int getSteepestIncline() {
        int steepestIncline = 0;
        for (Square square : getNeighbours()) {
            int incline = this.getHeight() - square.getHeight();
            if (steepestIncline < incline) {
                steepestIncline = incline;
            }
        }
        return steepestIncline;
    }

    /**
     * Find the squares with steepest incline, can be more then one.
     *
     * @return List with the square(s) with the steepest incline, returns null
     *         when there is no incline.
     */
    public Map<Direction, Square> getSquaresWithSteepestIncline() {
        int steepestIncline = getSteepestIncline();
        Map<Direction, Square> squaresWithSteepestIncline = null;

        if (steepestIncline > 0) {
            //steepest incline found, add all squares with this steepest incline
            squaresWithSteepestIncline = new HashMap<>();

            for (Square square : getNeighbours()) {
                if (this.getHeight() - square.getHeight() == steepestIncline) {
                    Direction key = Direction.NORTH;
                    if (square.equals(this.getNorth())) {
                        key = Direction.NORTH;
                    }
                    if (square.equals(this.getSouth())) {
                        key = Direction.SOUTH;
                    }
                    if (square.equals(this.getEast())) {
                        key = Direction.EAST;
                    }
                    if (square.equals(this.getWest())) {
                        key = Direction.WEST;
                    }

                    squaresWithSteepestIncline.put(key, square);
                }
            }
        }

        return squaresWithSteepestIncline;
    }

    public int getHeight() {
        return getZ();
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @return the z
     */
    public int getZ() {
        return z;
    }

    /**
     * @return the north square object
     */
    public Square getNorth() {
        return north;
    }

    /**
     * @param north the north square object to set
     */
    public void setNorth(Square north) {
        this.north = north;
    }

    /**
     * @return the south square object
     */
    public Square getSouth() {
        return south;
    }

    /**
     * @param south the south square object to set
     */
    public void setSouth(Square south) {
        this.south = south;
    }

    /**
     * @return the west square object
     */
    public Square getWest() {
        return west;
    }

    /**
     * @param west the west square object to set
     */
    public void setWest(Square west) {
        this.west = west;
    }

    /**
     * @return the east square object
     */
    public Square getEast() {
        return east;
    }

    /**
     * @param east the east square object to set
     */
    public void setEast(Square east) {
        this.east = east;
    }

    /**
     * set all neighbours
     */
    public void initNeighbours() {
        neighbours = new ArrayList();
        if (getNorth() != null) {
            getNeighbours().add(getNorth());
        }
        if (getSouth() != null) {
            getNeighbours().add(getSouth());
        }
        if (getEast() != null) {
            getNeighbours().add(getEast());
        }
        if (getWest() != null) {
            getNeighbours().add(getWest());
        }
    }

    /**
     * @return the water
     */
    public Water getWater() {
        return water;
    }

    /**
     * @param water the water to set
     */
    public void setWater(Water water) {
        this.water = water;
    }

    /**
     * @return the neighbours
     */
    public List<Square> getNeighbours() {
        return neighbours;
    }

    /**
     * waterLevel is the sum of square.height and water.height
     *
     * @return total height
     */
    public int getWaterLevel() {
        return this.getHeight() + this.getWater().getHeight();
    }

    @Override
    public int compareTo(Square square) {
        int compareWaterLevel = square.getWaterLevel();
        //Descending
        return compareWaterLevel - this.getWaterLevel();

        /* For Ascending order*/
        //return this.getWaterLevel()-compareWaterLevel;
    }

}
