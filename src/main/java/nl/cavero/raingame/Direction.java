package nl.cavero.raingame;

/**
 * water flows only in east-west (/west-east) and north-south (/south-north) 
 * direction. It does not flow diagonally over the region.
 * North - East - South - West
 */
public enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST
}
