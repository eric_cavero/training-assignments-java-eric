package nl.cavero.raingame;

import java.util.Map;

/**
 * Respresents the water. Like a chess piece. The rules of the flow are as
 * follows:
 *
 * The rules of the flow are as follows: -- water flows from high to low
 * regions. It never flows from low to high regions. -- water follows the
 * steepest incline, meaning that a region w ith an altitude of 8 next to a two
 * regions of 3 and 2 (resp.) will flow towards 2.
 *
 * -- if two inclines should be equally steep, the flow choses the first of
 * North - East - South - West. -- water flows only in east-west (/west-east)
 * and north-south (/south-north) direction. It does not flow diagonally over
 * the region. -- if the altitude of all neighbors is equal, there is no flow.
 */
public class Water {
    private final Square parent;
    private int height;
    
    public Water(Square parent) {
        this.parent = parent;
    }

    /**
     * determine applicable neighbours following the rules as listed above. Then
     * exchange water volume (height) with the applicable neighbour(s)
     *
     * @return the Square where the water flowed to, meaning:
     * returns the Square which water object has been received the water transfer.
     */
    public Square flow() {
        //water wants to flow, determine towards which neighbour
        //Is there an incline with neighbouring Squares?
        //Flow towards the steepest incline. Get all squares with an incline.
        Map<Direction, Square> squaresWithSteepestIncline
                = getParent().getSquaresWithSteepestIncline();

        //If squaresWithSteepestIncline == null there is no incline                
        Square destReturn = null;
        if (squaresWithSteepestIncline != null) {
            //if two inclines should be equally steep, the flow choses the 
            //first of North - East - South - West (enum Direction)
            for (Direction direction : Direction.values()) {
               Square destSquare = squaresWithSteepestIncline.get(direction);
                if (destSquare != null) {
                    transfer(destSquare);
                    destReturn = destSquare;
                }
            }
        }
        return destReturn;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    private void transfer(Square destSquare) {
        //destination square found adhering all rules,
        //flow water to found square.
        //water cannot rise higher then water level of source square (this),
         
        //height delta
        int heightDelta = this.getParent().getWaterLevel() - 
                destSquare.getWaterLevel();
        //the amount of water the destSquare can accept.
        int balance = heightDelta / 2;
        
        if(this.getHeight() >= balance) {
            //transfer all water destSquare can accept, source has enough water
            //available
            destSquare.getWater().increaseHight(balance);
            this.decreaseHight(balance);
        } else {
            //transfer all water the source square can give
            destSquare.getWater().increaseHight(this.getHeight());
            this.decreaseHight(this.getHeight());
        }
        
    }

    /**
     * @return the parent
     */
    public Square getParent() {
        return parent;
    }

    public void increaseHight(int height) {
        this.height += height;
    }
    
    public void decreaseHight(int height) {
        this.height -= height;
    }
}
