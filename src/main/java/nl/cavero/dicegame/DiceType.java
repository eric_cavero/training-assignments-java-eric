/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.cavero.dicegame;

/**
 *
 * @author Eric de Haas
 */
public enum DiceType {

  D2(2),
  D3(3),
  D4(4),
  D5(5),
  D6(6),
  D7(7),
  D8(8),
  D9(9),
  D10(10),
  D11(11),
  D12(12),
  D20(20),
  D22(22),
  D24(24),
  D30(30);

  private final int numberOfSides; 

  private DiceType(int numberOfSides) {
    this.numberOfSides = numberOfSides;
  }

  public int getNumberOfSides() {
    return numberOfSides;
  }
  
  public static final DiceType getByValue(int value) {
    for (DiceType type : values()) {
      if (type.numberOfSides == value) {
        return type;
      }
    }
    throw new IllegalArgumentException("No DiceType for value " + value);
  }
  
}
