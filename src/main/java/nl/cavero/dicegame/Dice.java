/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.cavero.dicegame;

/**
 *
 * @author Eric de Haas
 *
 * Simulates a dice
 * 
 * This dice object only simulates which side is rolled.
 * It does not say anything about the value(s) on the rolled side. This is to be 
 * determined by the user of this class.
 * Dice class needs array of DiceSides.
 * The class registers the number of rolls performed, until reset();
 * The class registers the last rolled side: getLastRoll();
 * The methode roll() returns a random DiceSide object.
 */
public class Dice {
  private final DiceSide[] diceSides;
  private final DiceType diceType;
  
  private int numberOfRolls;
  private DiceSide lastRoll;

  public Dice(DiceSide[] diceSides, DiceType diceType)  {
    this.diceSides = diceSides;
    this.diceType = diceType;
  }
  
  /**
   * Resets dice: numberOfRolls is set to 0, lastRoll is set to 0.
   * 
   * This implementation does not resets the number of diceType field.
   */
  public void reset() {
    resetNumberOfRolls();
    resetLastRoll();
  }
  
  /**
   * @return the numberOfRolls
   */
  public int getNumberOfRolls() {
    return numberOfRolls;
  }

  /**
   * numberOfRolls = 0
   */
  private void resetNumberOfRolls() {
    this.numberOfRolls = 0;
  }

  private void resetLastRoll() {
    lastRoll = null;
  }
  
  /**
   * increase numberOfRolls by 1
   */
  private void increaseNumberOfRolls() {
    this.numberOfRolls++;
  }

  /**
   * @return the lastRoll
   */
  public DiceSide getLastRoll() {
    return lastRoll;
  }

  
  /**
   * @param lastRoll the lastRoll to set
   */
  private void setLastRoll(DiceSide lastRoll) {
    this.lastRoll = lastRoll;
  }

  /**
   * 
   * @return the number of diceType
   */
  public DiceType getDiceType() {
    return this.diceType;
  }  
  
  /**
   * Generates a random value.
   * @return generated value, using Math.random()
   * 
   * This implementation generates a random number which is limited by: n >= 1 ;
   * n <= getDiceType(); Thus never 0.   
   */
  protected int generateRandomValue() {
    return (int) ( Math.random() * getDiceType().getNumberOfSides() + 1 );
  }

  /**
   * Increases the number of rolls 'field: numberOfRolls' by 1. Sets the field
   * lastRoll with the value of the random number.
   * @return the rolled value, thus the generated random value by methode:
   * generateRandomValue()
   *
   * This implementation generates a random number which is limited by: n >= 1 ;
   * n <= getSides(); Thus never 0. Override methode generateRandomValue() to
   * change behaviour of random values. See methode: generateRandomValue.   
   */
  public DiceSide roll() {
    increaseNumberOfRolls();
    this.setLastRoll( this.getDiceSideByIndex( this.generateRandomValue() - 1) );
    return this.getLastRoll();
  }

  private DiceSide getDiceSideByIndex(int index) {
    return diceSides[ index ];    
  }
}
