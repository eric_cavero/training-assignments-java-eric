package nl.cavero.dicegame;

/**
 * A simple dice game.
 *
 * <p>
 * The objective of this game is to find the number of dice rolls necessary to
 * get to the desired number. The game is played with two dice. The number found
 * should be printed on the screen.
 *
 * <p>
 * You are free to create as many methods and classes as you like to write up
 * the code. It's not necessary to restrict yourself to only this .
 * Additionally, you can use any library you find appropriate, except of course
 * one that would have this exact functionality...
 *
 * <p>
 * Remark: As this code is driven by (semi-)random behavior, i.e. the roll of
 * dice, we intend not to discuss the outcome, but the structure of the code and
 * the chosen solution method. We therefore haven't provided any test cases to
 * demonstrate the correct behavior.
 *
 */
public class DiceGame {

  private int desiredNumber;
  private Dice dice1;
  private Dice dice2;

  public DiceGame( Dice  dice1, Dice dice2 ) {
    this.dice1 = dice1;
    this.dice2 = dice2;
  }


  /**
   * @return the desiredNumber
   */
  public Integer getDesiredNumber() {
    return this.desiredNumber;
  }

  /**
   * @exception IllegalArgumentException is thrown when numberOfDiceSides < 2
   * @param desiredNumber the desiredNumber to set
   *
   * This implementation the desiredNumber is not allowed to be < 2 because
   * there are 2 dices and the minimum value per dice is 1. If the minimum value
   * of a dice changes to ie 0, this methode needs to be overridden.
   */
  public void setDesiredNumber(int desiredNumber) throws IllegalArgumentException {
    if (desiredNumber < 2) {
      throw new IllegalArgumentException("Desired number must be >= 2");
    }
    this.desiredNumber = desiredNumber;
  }

  /**
   * @return the dice1
   */
  public Dice getDice1() {
    return dice1;
  }

  /**
   * @param dice1 the dice1 to set
   */
  public void setDice1(Dice dice1) {
    this.dice1 = dice1;
  }

  /**
   * @return the dice2
   */
  public Dice getDice2() {
    return dice2;
  }

  /**
   * @param dice2 the dice2 to set
   */
  public void setDice2(Dice dice2) {
    this.dice2 = dice2;
  }

  /**
   *
   * @param desiredNumber The number the two dice must reach
   * @param diceSides
   * @return The number of dice rolls
   *
   * This implementation resets both dice: dice.lastRoll and dice.numberOfRolls
   * are both set to 0. The dice.numberOfSides is set.
   */
  public int start(Integer desiredNumber) throws IllegalArgumentException {
    this.getDice1().reset();
    this.getDice2().reset();

    try {
      this.setDesiredNumber(desiredNumber);
    } catch (IllegalArgumentException e) {
      throw (e);
    }

    playGame();
    return this.getNumberOfDiceRolls();
  }

  /**
   *
   * @return Number of rolled dice
   *
   * This implementation sums the number of rolls of both dice
   */
  public int getNumberOfDiceRolls() {
    return this.getDice1().getNumberOfRolls() + getDice2().getNumberOfRolls();
  }

  /**
   * Play the game: dice rolls necessary to get to the desired number
   *
   * This implementation rolls dice until accumulated dice results (tempNumber)
   * > desiredNumber. After that, reRoll the dice, until the excact
   * desiredNumber has been reached. Override this methode to implement a
   * different behaviour, for example: Roll until tempNumber > desiredNumber and
   * then stop rolling.
   */
  protected void playGame() {
    int tempNumber = 0;

    while (tempNumber != this.getDesiredNumber()) {

      if (tempNumber < this.getDesiredNumber()) {
        int numberBeforeRoll = tempNumber;
        tempNumber = roll(tempNumber);
        int numberAfterRoll = tempNumber;
        System.out.printf(
                "Desired number = %d, number before roll = %d, "
                + "number after roll = %d, rolled[%d, %d]%n",
                this.getDesiredNumber(), numberBeforeRoll, numberAfterRoll,
                this.dice1.getLastRoll().getValue(), 
                this.dice2.getLastRoll().getValue()
        );
      }

      if (tempNumber > this.getDesiredNumber()) {
        tempNumber = this.reRoll(tempNumber);
      }

      if (tempNumber + 1 == this.getDesiredNumber()) {
        //delta between tempNumber and this.getDesiredNumber should always be 
        //larger then 1. Because there are 2 dices.
        tempNumber = this.reRoll(tempNumber);
      }
    }
  }

  /**
   * Rolls the dice.
   *
   * @param value Result of previous rolls or reRolls
   * @return Sums the value of dice1 and dice2 and value and returns this
   *
   * This implementation rolls both dice and sums result dice1 + dice2 + value
   */
  protected int roll(int value) {
    int tempNumber = value;
    tempNumber += this.getDice1().roll().getValue();
    tempNumber += this.getDice2().roll().getValue();
    return tempNumber;
  }

  /**
   * Undo's the last roll on both dice
   *
   * @param value Result of previous rolls or reRolls
   * @return the value minus the result of the last roll of both dice
   *
   * This implementation undo's the last roll on both dice: tempNumber = value -
   * result last roll dice1 - result last roll dice2
   */
  protected int reRoll(int value) {
    int tempNumber = value;
    tempNumber -= this.getDice1().getLastRoll().getValue();
    tempNumber -= this.getDice2().getLastRoll().getValue();
    return tempNumber;
  }
}
