package nl.cavero.pyramid;

import org.junit.Test;

import static org.junit.Assert.*;

public class PyramidTest {

    @Test
    public void when_solving_the_pyramid_for_input_5_it_should_return_8_and_9() {
        int[] solution = new Pyramid(5).solve();
        assertEquals(8, solution[0]);
        assertEquals(9, solution[1]);
    }

}